//go:build !cgo || (!windows && !linux && !darwin)

package ft4222

import (
	"errors"
)

var ErrUnsupportedPlatform = errors.New("ft4222: unsupported platform")

func CreateDeviceInfoList() (uint, error) {
	return 0, ErrUnsupportedPlatform
}

func GetDeviceInfoDetail(nb uint) (*DeviceInfo, error) {
	return nil, ErrUnsupportedPlatform
}

func GetDeviceInfoList(ft4222_only bool) ([]*DeviceInfo, error) {
	return nil, ErrUnsupportedPlatform
}

func OpenBySerial(serial string) (*Ft4222, error) {
	return nil, ErrUnsupportedPlatform
}

func OpenByDescription(desc string) (*Ft4222, error) {
	return nil, ErrUnsupportedPlatform
}

// """Open a handle to a usb device by location
func OpenByLocation(locId uint) (*Ft4222, error) {
	return nil, ErrUnsupportedPlatform
}

func (f *Ft4222) Close() error {
	return ErrUnsupportedPlatform
}

func (f *Ft4222) ChipVersion() uint {
	return 0
}

func (f *Ft4222) LibVersion() uint {
	return 0
}

func (f *Ft4222) ChipRevision() string {
	return ""
}

func (f *Ft4222) SetTimeouts(read_timeout, write_timeout uint) error {
	return ErrUnsupportedPlatform
}

func (f *Ft4222) SetClock(clk SysClock) error {
	return ErrUnsupportedPlatform
}

func (f *Ft4222) GetClock() (SysClock, error) {
	return 0, ErrUnsupportedPlatform
}

func (f *Ft4222) I2cMaster_Init(kbps int) error {
	return ErrUnsupportedPlatform
}

func (f *Ft4222) I2cMaster_Read(addr uint16, buf []byte) (int, error) {
	return 0, ErrUnsupportedPlatform
}

func (f *Ft4222) I2cMaster_Write(addr uint16, data []byte) (int, error) {
	return 0, ErrUnsupportedPlatform
}

func (f *Ft4222) I2cMaster_ReadEx(addr uint16, buf []byte, flag uint8) (int, error) {
	return 0, ErrUnsupportedPlatform
}

func (f *Ft4222) I2cMaster_WriteEx(addr uint16, data []byte, flag uint8) (int, error) {
	return 0, ErrUnsupportedPlatform
}

func (f *Ft4222) I2cMaster_Reset() error {
	return ErrUnsupportedPlatform
}

func (f *Ft4222) I2cMaster_ResetBus() error {
	return ErrUnsupportedPlatform
}

func (f *Ft4222) I2cMaster_GetStatus() (uint8, error) {
	return 0, ErrUnsupportedPlatform
}

func (f *Ft4222) I2cSlave_Init() error {
	return ErrUnsupportedPlatform
}

func (f *Ft4222) I2cSlave_Reset() error {
	return ErrUnsupportedPlatform
}

func (f *Ft4222) I2cSlave_GetAddress() (uint8, error) {
	return 0, ErrUnsupportedPlatform
}

func (f *Ft4222) I2cSlave_SetAddress(addr uint8) error {
	return ErrUnsupportedPlatform
}

func (f *Ft4222) I2cSlave_GetRxStatus() (uint16, error) {
	return 0, ErrUnsupportedPlatform
}

func (f *Ft4222) I2cSlave_Read(buf []byte) error {
	return ErrUnsupportedPlatform
}

func (f *Ft4222) I2cSlave_Write(data []byte) (uint, error) {
	return 0, ErrUnsupportedPlatform
}

func (f *Ft4222) I2CSlave_SetClockStretch(enable bool) error {
	return ErrUnsupportedPlatform
}

func (f *Ft4222) I2CSlave_SetRespWord(responseWord uint8) error {
	return ErrUnsupportedPlatform
}

type GPIOTrigger uint
type GPIODir uint
type GPIOPort uint
type GPIOValue uint

func (f *Ft4222) GPIO_Init(isInput [4]GPIODir) error {
	return ErrUnsupportedPlatform
}

func (f *Ft4222) GPIO_Read(portNum GPIOPort) (GPIOValue, error) {
	return 0, ErrUnsupportedPlatform
}

func (f *Ft4222) GPIO_Write(portNum GPIOPort, val GPIOValue) error {
	return ErrUnsupportedPlatform
}

func (f *Ft4222) GPIO_SetInputTrigger(portNum GPIOPort, trigger GPIOTrigger) error {
	return ErrUnsupportedPlatform
}

func (f *Ft4222) GPIO_GetTriggerStatus(portNum GPIOPort) (uint16, error) {
	return 0, ErrUnsupportedPlatform
}

func (f *Ft4222) GPIO_ReadTriggerQueue(portNum GPIOPort, events []GPIOTrigger) (int, error) {
	return 0, ErrUnsupportedPlatform
}

func (f *Ft4222) GPIO_SetWaveFormMode(enable bool) error {
	return ErrUnsupportedPlatform
}
