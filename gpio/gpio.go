package gpio

import (
	"strings"
	"unsafe"

	"gitlab.com/msrelectronics/go-ft4222"
)

type GPIO struct {
	*ft4222.Ft4222
}

const (
	Output = Direction(0)
	Input  = Direction(1)
)

type Direction ft4222.GPIODir

func (d Direction) String() string {
	switch d {
	case Output:
		return "output"
	case Input:
		return "input"
	default:
		return ""
	}
}

const (
	Port1 = Port(0)
	Port2 = Port(1)
	Port3 = Port(3)
	Port4 = Port(4)
)

type Port ft4222.GPIOPort

func (p Port) String() string {
	switch p {
	case Port1:
		return "Port 1"
	case Port2:
		return "Port 2"
	case Port3:
		return "Port 3"
	case Port4:
		return "Port 4"
	default:
		return ""
	}
}

const (
	Low  = Value(0)
	High = Value(1)
)

type Value ft4222.GPIOValue

func (v Value) String() string {
	switch v {
	case Low:
		return "low"
	case High:
		return "high"
	default:
		return ""
	}
}

const (
	TriggerRising    = Trigger(0x01)
	TriggerFalling   = Trigger(0x02)
	TriggerLevelHigh = Trigger(0x04)
	TriggerLevelLow  = Trigger(0x08)
)

type Trigger ft4222.GPIOTrigger

func (t Trigger) String() string {
	s := ""
	if t&TriggerRising != 0 {
		s += "|TriggerRising"
	}
	if t&TriggerFalling != 0 {
		s += "|TriggerFalling"
	}
	if t&TriggerLevelHigh != 0 {
		s += "|TriggerLevelHigh"
	}
	if t&TriggerLevelLow != 0 {
		s += "|TriggerLevelLow"
	}

	return strings.TrimLeft(s, "|")
}

func Init(f *ft4222.Ft4222, port1, port2, port3, port4 Direction) (*GPIO, error) {
	d := [4]ft4222.GPIODir{ft4222.GPIODir(port1), ft4222.GPIODir(port2), ft4222.GPIODir(port3), ft4222.GPIODir(port4)}
	if err := f.GPIO_Init(d); err != nil {
		return nil, err
	}

	return &GPIO{f}, nil
}

func (f *GPIO) Read(port Port) (Value, error) {
	v, err := f.GPIO_Read(ft4222.GPIOPort(port))
	return Value(v), err
}

func (f *GPIO) Write(port Port, val Value) error {
	return f.GPIO_Write(ft4222.GPIOPort(port), ft4222.GPIOValue(val))
}

func (f *GPIO) SetInputTrigger(portNum Port, trigger Trigger) error {
	return f.GPIO_SetInputTrigger(ft4222.GPIOPort(portNum), ft4222.GPIOTrigger(trigger))
}

func (f *GPIO) GetTriggerStatus(portNum Port) (uint, error) {
	s, err := f.GPIO_GetTriggerStatus(ft4222.GPIOPort(portNum))
	return uint(s), err
}

func (f *GPIO) ReadTriggerQueue(portNum Port, events []Trigger) (int, error) {
	return f.GPIO_ReadTriggerQueue(ft4222.GPIOPort(portNum), *(*[]ft4222.GPIOTrigger)(unsafe.Pointer(&events)))
}

func (f *GPIO) SetWaveFormMode(enable bool) error {
	return f.GPIO_SetWaveFormMode(enable)
}
