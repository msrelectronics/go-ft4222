package ft4222

import (
	"unsafe"

	_ "gitlab.com/msrelectronics/go-ft4222/lib"
)

const maxDescriptionSize = 256

var chipRevMap = map[uint]string{
	0x42220100: "Rev. A",
	0x42220200: "Rev. B",
	0x42220300: "Rev. C",
	0x42220400: "Rev. D",
}

type DeviceInfo struct {
	Index       uint
	Flags       uint
	Type        uint
	Id          uint
	Location    uint
	Serial      string
	Description string
	Handle      uintptr
}

type Ft4222 struct {
	handle      unsafe.Pointer
	chipVersion uint
	dllVersion  uint
}

type SysClock uint

const (
	SysClock_60MHz = 0
	SysClock_24MHz = 1
	SysClock_48MHz = 2
	SysClock_80MHz = 3
)

func (s SysClock) String() string {
	switch s {
	case SysClock_60MHz:
		return "60Mhz"
	case SysClock_24MHz:
		return "24MHz"
	case SysClock_48MHz:
		return "48MHz"
	case SysClock_80MHz:
		return "80MHz"
	default:
		return ""
	}
}
