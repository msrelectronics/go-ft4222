//go:build cgo && (windows || linux || darwin)

package ft4222

/*
#cgo CFLAGS: -I./lib/.
#cgo CFLAGS: -DFT4222_STATIC -DFTD2XX_STATIC

#include <stdlib.h>
#include "ftd2xx.h"
#include "libft4222.h"
*/
import "C"
import (
	"fmt"
	"math"
	"runtime"
	"unsafe"
)

var ftd2xxErrorMap = map[uint]string{
	C.FT_OK:                          "FT_OK",
	C.FT_INVALID_HANDLE:              "FT_INVALID_HANDLE",
	C.FT_DEVICE_NOT_FOUND:            "FT_DEVICE_NOT_FOUND",
	C.FT_DEVICE_NOT_OPENED:           "FT_DEVICE_NOT_OPENED",
	C.FT_IO_ERROR:                    "FT_IO_ERROR",
	C.FT_INSUFFICIENT_RESOURCES:      "FT_INSUFFICIENT_RESOURCES",
	C.FT_INVALID_PARAMETER:           "FT_INVALID_PARAMETER",
	C.FT_INVALID_BAUD_RATE:           "FT_INVALID_BAUD_RATE",
	C.FT_DEVICE_NOT_OPENED_FOR_ERASE: "FT_DEVICE_NOT_OPENED_FOR_ERASE",
	C.FT_DEVICE_NOT_OPENED_FOR_WRITE: "FT_DEVICE_NOT_OPENED_FOR_WRITE",
	C.FT_FAILED_TO_WRITE_DEVICE:      "FT_FAILED_TO_WRITE_DEVICE",
	C.FT_EEPROM_READ_FAILED:          "FT_EEPROM_READ_FAILED",
	C.FT_EEPROM_WRITE_FAILED:         "FT_EEPROM_WRITE_FAILED",
	C.FT_EEPROM_ERASE_FAILED:         "FT_EEPROM_ERASE_FAILED",
	C.FT_EEPROM_NOT_PRESENT:          "FT_EEPROM_NOT_PRESENT",
	C.FT_EEPROM_NOT_PROGRAMMED:       "FT_EEPROM_NOT_PROGRAMMED",
	C.FT_INVALID_ARGS:                "FT_INVALID_ARGS",
	C.FT_NOT_SUPPORTED:               "FT_NOT_SUPPORTED",
	C.FT_OTHER_ERROR:                 "FT_OTHER_ERROR",
	C.FT_DEVICE_LIST_NOT_READY:       "FT_DEVICE_LIST_NOT_READY",
}

type ftError struct {
	Nb uint
}

func (e *ftError) Error() string {
	s, ok := ftd2xxErrorMap[e.Nb]
	if !ok {
		return "unknown error"
	}
	return s
}

const ft4222ErrorStart = 1000
const deviceTypeFt4222 = 10

var ft4222ErrorMap map[uint]string = map[uint]string{
	C.FT4222_DEVICE_NOT_SUPPORTED:                 "FT4222_DEVICE_NOT_SUPPORTED",
	C.FT4222_CLK_NOT_SUPPORTED:                    "FT4222_CLK_NOT_SUPPORTED",
	C.FT4222_VENDER_CMD_NOT_SUPPORTED:             "FT4222_VENDER_CMD_NOT_SUPPORTED",
	C.FT4222_IS_NOT_SPI_MODE:                      "FT4222_IS_NOT_SPI_MODE",
	C.FT4222_IS_NOT_I2C_MODE:                      "FT4222_IS_NOT_I2C_MODE",
	C.FT4222_IS_NOT_SPI_SINGLE_MODE:               "FT4222_IS_NOT_SPI_SINGLE_MODE",
	C.FT4222_IS_NOT_SPI_MULTI_MODE:                "FT4222_IS_NOT_SPI_MULTI_MODE",
	C.FT4222_WRONG_I2C_ADDR:                       "FT4222_WRONG_I2C_ADDR",
	C.FT4222_INVAILD_FUNCTION:                     "FT4222_INVAILD_FUNCTION",
	C.FT4222_INVALID_POINTER:                      "FT4222_INVALID_POINTER",
	C.FT4222_EXCEEDED_MAX_TRANSFER_SIZE:           "FT4222_EXCEEDED_MAX_TRANSFER_SIZE",
	C.FT4222_FAILED_TO_READ_DEVICE:                "FT4222_FAILED_TO_READ_DEVICE",
	C.FT4222_I2C_NOT_SUPPORTED_IN_THIS_MODE:       "FT4222_I2C_NOT_SUPPORTED_IN_THIS_MODE",
	C.FT4222_GPIO_NOT_SUPPORTED_IN_THIS_MODE:      "FT4222_GPIO_NOT_SUPPORTED_IN_THIS_MODE",
	C.FT4222_GPIO_EXCEEDED_MAX_PORTNUM:            "FT4222_GPIO_EXCEEDED_MAX_PORTNUM",
	C.FT4222_GPIO_WRITE_NOT_SUPPORTED:             "FT4222_GPIO_WRITE_NOT_SUPPORTED",
	C.FT4222_GPIO_PULLUP_INVALID_IN_INPUTMODE:     "FT4222_GPIO_PULLUP_INVALID_IN_INPUTMODE",
	C.FT4222_GPIO_PULLDOWN_INVALID_IN_INPUTMODE:   "FT4222_GPIO_PULLDOWN_INVALID_IN_INPUTMODE",
	C.FT4222_GPIO_OPENDRAIN_INVALID_IN_OUTPUTMODE: "FT4222_GPIO_OPENDRAIN_INVALID_IN_OUTPUTMODE",
	C.FT4222_INTERRUPT_NOT_SUPPORTED:              "FT4222_INTERRUPT_NOT_SUPPORTED",
	C.FT4222_GPIO_INPUT_NOT_SUPPORTED:             "FT4222_GPIO_INPUT_NOT_SUPPORTED",
	C.FT4222_EVENT_NOT_SUPPORTED:                  "FT4222_EVENT_NOT_SUPPORTED",
	C.FT4222_FUN_NOT_SUPPORT:                      "FT4222_FUN_NOT_SUPPORT",
}

type ft4222Error struct {
	ftError
}

func (e *ft4222Error) Error() string {
	if e.Nb < ft4222ErrorStart {
		return e.ftError.Error()
	}
	s, ok := ft4222ErrorMap[e.Nb]
	if !ok {
		return "unknown error"
	}
	return s
}

func newError(nb uint) error {
	if nb < ft4222ErrorStart {
		return &ftError{Nb: nb}
	}
	return &ft4222Error{ftError: ftError{Nb: nb}}
}

func (f *Ft4222) ftHandle() C.FT_HANDLE {
	return C.FT_HANDLE(f.handle)
}

func CreateDeviceInfoList() (uint, error) {
	var nb C.DWORD
	s := C.FT_CreateDeviceInfoList(&nb)
	if s != C.FT_OK {
		return 0, newError(uint(s))
	}
	return uint(nb), nil
}

func GetDeviceInfoDetail(nb uint) (*DeviceInfo, error) {
	var f, t, i, l C.DWORD
	var h C.FT_HANDLE
	var n [maxDescriptionSize]C.char
	var d [maxDescriptionSize]C.char

	s := C.FT_GetDeviceInfoDetail(C.DWORD(nb), &f, &t, &i, &l, C.LPVOID(&n[0]), C.LPVOID(&d[0]), &h)
	if s != C.FT_OK {
		return nil, newError(uint(s))
	}

	return &DeviceInfo{
		Index:       nb,
		Flags:       uint(f),
		Type:        uint(t),
		Id:          uint(i),
		Location:    uint(l),
		Serial:      C.GoString(&n[0]),
		Description: C.GoString(&d[0]),
		Handle:      uintptr(h),
	}, nil
}

func GetDeviceInfoList(ft4222_only bool) ([]*DeviceInfo, error) {
	n, err := CreateDeviceInfoList()
	if err != nil {
		return nil, err
	}

	l := make([]*DeviceInfo, 0, n)

	for i := uint(0); i < n; i++ {
		d, err := GetDeviceInfoDetail(i)
		if err != nil {
			return nil, err
		}
		if ft4222_only {
			if d.Type == deviceTypeFt4222 {
				l = append(l, d)
			}
		} else {
			l = append(l, d)
		}
	}

	return l, nil
}

func newFt4222(handle C.FT_HANDLE) (*Ft4222, error) {
	f := &Ft4222{
		handle: unsafe.Pointer(handle),
	}

	if err := f.getVersion(); err != nil {
		return nil, err
	}

	return f, nil
}

func open(handle C.FT_HANDLE) (*Ft4222, error) {
	f, err := newFt4222(handle)
	if err != nil {
		return nil, err
	}

	runtime.SetFinalizer(f, func(f *Ft4222) {
		f.Close()
	})

	return f, nil
}

func OpenBySerial(serial string) (*Ft4222, error) {
	cstr := C.CString(serial)
	defer C.free(unsafe.Pointer(cstr))

	var handle C.FT_HANDLE
	s := C.FT_OpenEx(C.PVOID(cstr), C.FT_OPEN_BY_SERIAL_NUMBER, &handle)
	if s != C.FT_OK {
		return nil, newError(uint(s))
	}

	return open(handle)
}

func OpenByDescription(desc string) (*Ft4222, error) {
	cstr := C.CString(desc)
	defer C.free(unsafe.Pointer(cstr))

	var handle C.FT_HANDLE
	s := C.FT_OpenEx(C.PVOID(cstr), C.FT_OPEN_BY_DESCRIPTION, &handle)
	if s != C.FT_OK {
		return nil, newError(uint(s))
	}

	return open(handle)
}

// """Open a handle to a usb device by location
func OpenByLocation(locId uint) (*Ft4222, error) {
	var handle C.FT_HANDLE
	s := C.FT_OpenEx(C.PVOID(uintptr(locId)), C.FT_OPEN_BY_LOCATION, &handle)
	if s != C.FT_OK {
		return nil, newError(uint(s))
	}

	return open(handle)
}

func (f *Ft4222) String() string {
	return fmt.Sprintf("FT4222: chipVersion: 0x%x (%s), libVersion: 0x%x", f.chipVersion, f.ChipRevision(), f.dllVersion)
}

func (f *Ft4222) Close() error {
	if f.handle == nil {
		return nil
	}

	C.FT4222_UnInitialize(f.ftHandle())
	s := C.FT_Close(f.ftHandle())
	if s != C.FT_OK {
		return newError(uint(s))
	}
	f.handle = nil
	return nil
}

func (f *Ft4222) getVersion() error {
	var ver C.FT4222_Version
	s := C.FT4222_GetVersion(f.ftHandle(), &ver)
	if s != C.FT_OK {
		return newError(uint(s))
	}
	f.chipVersion = uint(ver.chipVersion)
	f.dllVersion = uint(ver.dllVersion)
	return nil
}

func (f *Ft4222) ChipVersion() uint {
	return f.chipVersion
}

func (f *Ft4222) LibVersion() uint {
	return f.dllVersion
}

func (f *Ft4222) ChipRevision() string {
	if s, ok := chipRevMap[f.chipVersion]; ok {
		return s
	}
	return "unknown revision"
}

func (f *Ft4222) SetTimeouts(read_timeout, write_timeout uint) error {
	s := C.FT_SetTimeouts(f.ftHandle(), C.ULONG(read_timeout), C.ULONG(write_timeout))
	if s != C.FT_OK {
		return newError(uint(s))
	}
	return nil
}

func (f *Ft4222) SetClock(clk SysClock) error {
	s := C.FT4222_SetClock(f.ftHandle(), C.FT4222_ClockRate(clk))
	if s != C.FT_OK {
		return newError(uint(s))
	}
	return nil
}

func (f *Ft4222) GetClock() (SysClock, error) {
	var clk C.FT4222_ClockRate
	s := C.FT4222_GetClock(f.ftHandle(), &clk)
	if s != C.FT_OK {
		return 0, newError(uint(s))
	}
	return SysClock(clk), nil
}

func (f *Ft4222) vendorCmdSet(req byte, data []byte) error {
	s := C.FT_VendorCmdSet(f.ftHandle(), C.uchar(req), (*C.uchar)(&data[0]), C.ushort(len(data)))
	if s != C.FT_OK {
		return newError(uint(s))
	}
	return nil
}

func (f *Ft4222) I2cMaster_Init(kbps int) error {
	if kbps < 0 {
		kbps = 100
	}

	s := C.FT4222_I2CMaster_Init(f.ftHandle(), C.uint(kbps))
	if s != C.FT_OK {
		return newError(uint(s))
	}

	// current version (v1.3) of ftdi's lib can only handle clock rates down to 60kHz
	// although the chip can handle clock rates down to ~23.6kHz
	// there's a undocumented ways to achieve this
	if kbps < 60 {
		//             Operating Clock Freq
		// SCL Freq = -----------------------  | M = 6 or 8; N = 1, 2, 3, …, 127
		//                 M*(N+1)
		//
		//       Operating Clock Freq             24MHz
		// N =  ---------------------- - 1   => ---------- - 1
		//            M * SCL Freq               8 * kbps
		//
		n := byte(max(min(int(math.Round(24000000.0/(8*1000*float64(kbps))-1)), 127), 1))
		if err := f.SetClock(SysClock_24MHz); err != nil {
			return err
		}
		if err := f.vendorCmdSet(0x52, []byte{n}); err != nil {
			return err
		}
	}

	return nil
}

func (f *Ft4222) I2cMaster_Read(addr uint16, buf []byte) (int, error) {
	var bytesRead C.uint16

	s := C.FT4222_I2CMaster_Read(f.ftHandle(), C.uint16(addr), (*C.uint8)(&buf[0]), C.uint16(len(buf)), &bytesRead)
	if s != C.FT_OK {
		return 0, newError(uint(s))
	}

	return int(bytesRead), nil
}

func (f *Ft4222) I2cMaster_Write(addr uint16, data []byte) (int, error) {
	var bytesSent C.uint16

	s := C.FT4222_I2CMaster_Write(f.ftHandle(), C.uint16(addr), (*C.uint8)(&data[0]), C.uint16(len(data)), &bytesSent)
	if s != C.FT_OK {
		return 0, newError(uint(s))
	}

	return int(bytesSent), nil
}

func (f *Ft4222) I2cMaster_ReadEx(addr uint16, buf []byte, flag uint8) (int, error) {
	var bytesRead C.uint16

	s := C.FT4222_I2CMaster_ReadEx(f.ftHandle(), C.uint16(addr), C.uint8(flag), (*C.uint8)(&buf[0]), C.uint16(len(buf)), &bytesRead)
	if s != C.FT_OK {
		return 0, newError(uint(s))
	}

	return int(bytesRead), nil
}

func (f *Ft4222) I2cMaster_WriteEx(addr uint16, data []byte, flag uint8) (int, error) {
	var bytesSent C.uint16

	s := C.FT4222_I2CMaster_WriteEx(f.ftHandle(), C.uint16(addr), C.uint8(flag), (*C.uint8)(&data[0]), C.uint16(len(data)), &bytesSent)
	if s != C.FT_OK {
		return 0, newError(uint(s))
	}

	return int(bytesSent), nil
}

func (f *Ft4222) I2cMaster_Reset() error {
	s := C.FT4222_I2CMaster_Reset(f.ftHandle())
	if s != C.FT_OK {
		return newError(uint(s))
	}
	return nil
}

func (f *Ft4222) I2cMaster_ResetBus() error {
	s := C.FT4222_I2CMaster_ResetBus(f.ftHandle())
	if s != C.FT_OK {
		return newError(uint(s))
	}
	return nil
}

func (f *Ft4222) I2cMaster_GetStatus() (uint8, error) {
	var st C.uint8
	s := C.FT4222_I2CMaster_GetStatus(f.ftHandle(), &st)
	if s != C.FT_OK {
		return 0, newError(uint(s))
	}
	return uint8(st), nil
}

func (f *Ft4222) I2cSlave_Init() error {
	s := C.FT4222_I2CSlave_Init(f.ftHandle())
	if s != C.FT_OK {
		return newError(uint(s))
	}
	return nil
}

func (f *Ft4222) I2cSlave_Reset() error {
	s := C.FT4222_I2CSlave_Reset(f.ftHandle())
	if s != C.FT_OK {
		return newError(uint(s))
	}
	return nil
}

func (f *Ft4222) I2cSlave_GetAddress() (uint8, error) {
	var addr C.uint8
	s := C.FT4222_I2CSlave_GetAddress(f.ftHandle(), &addr)
	if s != C.FT_OK {
		return 0, newError(uint(s))
	}
	return uint8(addr), nil
}

func (f *Ft4222) I2cSlave_SetAddress(addr uint8) error {
	s := C.FT4222_I2CSlave_SetAddress(f.ftHandle(), C.uint8(addr))
	if s != C.FT_OK {
		return newError(uint(s))
	}
	return nil
}

func (f *Ft4222) I2cSlave_GetRxStatus() (uint16, error) {
	var status C.uint16
	s := C.FT4222_I2CSlave_GetRxStatus(f.ftHandle(), &status)
	if s != C.FT_OK {
		return 0, newError(uint(s))
	}
	return uint16(status), nil
}

func (f *Ft4222) I2cSlave_Read(buf []byte) (int, error) {
	var bytesRead C.uint16

	s := C.FT4222_I2CSlave_Read(f.ftHandle(), (*C.uint8)(&buf[0]), C.uint16(len(buf)), &bytesRead)
	if s != C.FT_OK {
		return int(bytesRead), newError(uint(s))
	}

	return int(bytesRead), nil
}

func (f *Ft4222) I2cSlave_Write(data []byte) (int, error) {
	var bytesSent C.uint16

	s := C.FT4222_I2CSlave_Write(f.ftHandle(), (*C.uint8)(&data[0]), C.uint16(len(data)), &bytesSent)
	if s != C.FT_OK {
		return 0, newError(uint(s))
	}

	return int(bytesSent), nil
}

func (f *Ft4222) I2CSlave_SetClockStretch(enable bool) error {
	e := C.BOOL(0)
	if enable {
		e = C.BOOL(1)
	}
	s := C.FT4222_I2CSlave_SetClockStretch(f.ftHandle(), e)
	if s != C.FT_OK {
		return newError(uint(s))
	}
	return nil
}

func (f *Ft4222) I2CSlave_SetRespWord(responseWord uint8) error {
	s := C.FT4222_I2CSlave_SetRespWord(f.ftHandle(), C.uint8(responseWord))
	if s != C.FT_OK {
		return newError(uint(s))
	}
	return nil
}

type GPIOTrigger C.GPIO_Trigger
type GPIODir C.GPIO_Dir
type GPIOPort C.GPIO_Port
type GPIOValue C.BOOL

func (f *Ft4222) GPIO_Init(isInput [4]GPIODir) error {
	s := C.FT4222_GPIO_Init(f.ftHandle(), (*C.GPIO_Dir)(&isInput[0]))
	if s != C.FT_OK {
		return newError(uint(s))
	}
	return nil
}

func (f *Ft4222) GPIO_Read(portNum GPIOPort) (GPIOValue, error) {
	var val C.BOOL
	s := C.FT4222_GPIO_Read(f.ftHandle(), C.GPIO_Port(portNum), &val)
	if s != C.FT_OK {
		return 0, newError(uint(s))
	}
	return GPIOValue(val), nil
}

func (f *Ft4222) GPIO_Write(portNum GPIOPort, val GPIOValue) error {
	s := C.FT4222_GPIO_Write(f.ftHandle(), C.GPIO_Port(portNum), C.BOOL(val))
	if s != C.FT_OK {
		return newError(uint(s))
	}
	return nil
}

func (f *Ft4222) GPIO_SetInputTrigger(portNum GPIOPort, trigger GPIOTrigger) error {
	s := C.FT4222_GPIO_SetInputTrigger(f.ftHandle(), C.GPIO_Port(portNum), C.GPIO_Trigger(trigger))
	if s != C.FT_OK {
		return newError(uint(s))
	}
	return nil
}

func (f *Ft4222) GPIO_GetTriggerStatus(portNum GPIOPort) (uint16, error) {
	var st C.uint16
	s := C.FT4222_GPIO_GetTriggerStatus(f.ftHandle(), C.GPIO_Port(portNum), &st)
	if s != C.FT_OK {
		return 0, newError(uint(s))
	}
	return uint16(st), nil
}

func (f *Ft4222) GPIO_ReadTriggerQueue(portNum GPIOPort, events []GPIOTrigger) (int, error) {
	var eventsRead C.uint16

	s := C.FT4222_GPIO_ReadTriggerQueue(f.ftHandle(), C.GPIO_Port(portNum), (*C.GPIO_Trigger)(&events[0]), C.uint16(len(events)), &eventsRead)
	if s != C.FT_OK {
		return 0, newError(uint(s))
	}

	return int(eventsRead), nil
}

func (f *Ft4222) GPIO_SetWaveFormMode(enable bool) error {
	var e C.BOOL
	if enable {
		e = C.BOOL(1)
	}
	s := C.FT4222_GPIO_SetWaveFormMode(f.ftHandle(), e)
	if s != C.FT_OK {
		return newError(uint(s))
	}
	return nil
}
