package i2cm

import (
	"gitlab.com/msrelectronics/go-ft4222"
)

type I2CMaster struct {
	*ft4222.Ft4222
}

func Init(f *ft4222.Ft4222, kbps int) (*I2CMaster, error) {
	if err := f.I2cMaster_Init(kbps); err != nil {
		return nil, err
	}

	return &I2CMaster{f}, nil
}

func (f *I2CMaster) Read(addr uint16, buf []byte) (int, error) {
	return f.I2cMaster_Read(addr, buf)
}

func (f *I2CMaster) Write(addr uint16, data []byte) (int, error) {
	return f.I2cMaster_Write(addr, data)
}

type Flag uint8

const (
	FlagNone          = Flag(0x80)
	FlagStart         = Flag(0x02)
	FlagRepeatedStart = Flag(0x03) // Repeated_START will not send master code in HS mode
	FlagStop          = Flag(0x04)
	FlagStartAndStop  = Flag(0x06)
)

func (f *I2CMaster) ReadEx(addr uint16, buf []byte, flag Flag) (int, error) {
	return f.I2cMaster_ReadEx(addr, buf, uint8(flag))
}

func (f *I2CMaster) WriteEx(addr uint16, data []byte, flag Flag) (int, error) {
	return f.I2cMaster_WriteEx(addr, data, uint8(flag))
}

func (f *I2CMaster) Reset() error {
	return f.I2cMaster_Reset()
}

func (f *I2CMaster) ResetBus() error {
	return f.I2cMaster_ResetBus()
}

type Status uint8

const (
	StatusControllerBusy  = Status(1 << 0) // all other status bits invalid
	StatusError           = Status(1 << 1) // error condition
	StatusAddrNack        = Status(1 << 2) // slave address was not acknowledged during last operation
	StatusDataNack        = Status(1 << 3) // data not acknowledged during last operation
	StatusArbitrationLost = Status(1 << 4) // arbitration lost during last operation
	StatusIdle            = Status(1 << 5) // controller idle
	StatusBusBusy         = Status(1 << 6) // bus busy
)

func (s Status) ControllerBusy() bool {
	return (s & 0x01) != 0
}
func (s Status) DataNack() bool {
	return (s & 0x0A) != 0
}
func (s Status) AddressNack() bool {
	return (s & 0x06) != 0
}
func (s Status) ArbitrationLost() bool {
	return (s & 0x12) != 0
}
func (s Status) Idle() bool {
	return (s & 0x20) != 0
}
func (s Status) BusBusy() bool {
	return (s & 0x40) != 0
}

func (f *I2CMaster) GetStatus() (Status, error) {
	s, err := f.I2cMaster_GetStatus()
	return Status(s), err
}
