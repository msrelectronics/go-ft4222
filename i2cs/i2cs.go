package i2cs

import "gitlab.com/msrelectronics/go-ft4222"

type I2CSlave struct {
	*ft4222.Ft4222
}

func Init(f *ft4222.Ft4222) (*I2CSlave, error) {
	if err := f.I2cSlave_Init(); err != nil {
		return nil, err
	}

	return &I2CSlave{f}, nil
}

func (f *I2CSlave) Reset() error {
	return f.I2cSlave_Reset()
}
func (f *I2CSlave) GetAddress() (uint8, error) {
	return f.I2cSlave_GetAddress()
}
func (f *I2CSlave) SetAddress(addr uint8) error {
	return f.I2cSlave_SetAddress(addr)
}
func (f *I2CSlave) GetRxStatus() (uint, error) {
	s, err := f.I2cSlave_GetRxStatus()
	return uint(s), err
}
func (f *I2CSlave) Read(buf []byte) (int, error) {
	return f.I2cSlave_Read(buf)
}
func (f *I2CSlave) Write(data []byte) (int, error) {
	return f.I2cSlave_Write(data)
}
func (f *I2CSlave) SetClockStretch(enable bool) error {
	return f.I2CSlave_SetClockStretch(enable)
}
func (f *I2CSlave) SetRespWord(responseWord uint8) error {
	return f.I2CSlave_SetRespWord(responseWord)
}
